# zfstozab

This gem installs an executable - zabbix-zfs.rb - that pushes zfs procfs data to zabbix.  It is built on the [zabbix_sender_api](https://gitlab.com/svdasein/zabbix_sender_api) gem, which will be installed as a dependency.  The Zabbix template is in this git repository as zabbix-template-zfs.xml.  Just download and import it.

Note that there are some items in the procfs data that I'm unclear on. You'll see that in the template (e.g. rupdate, wlentime...).  I rendered them as change per second values because else they're just gigantic counter looking things, and they do appear to go up and down when treated as a rate.  Corrections are welcome.

## Installation

    $ gem install zfstozab

## Usage

```
zabbix-zfs.rb --help
Options:
  -p, --procfsdir=<s>     Path to the root of the zfs proc stuff (default: /proc/spl/kstat/zfs)
  -h, --hostname=<s>      Name of host being monitored (default: fileserver.mynet.org)
  -r, --proxy=<s>         Proxy to send data to (default: 192.168.1.1)
  -s, --senderpath=<s>    Path to zabbix_sender (default: zabbix_sender)
  -d, --dump              
  -e, --help              Show this message
```


Invocation can be as simple as:

```
root@myserver:~# zabbix-zfs.rb 
zabbix_sender [2020115]: DEBUG: answer [{"response":"success","info":"processed: 137; failed: 0; total: 137; seconds spent: 0.072812"}]
info from server: "processed: 137; failed: 0; total: 137; seconds spent: 0.072812"
sent: 137; skipped: 0; total: 137
```
... presuming that your zabbix_agentd.conf is in one of the usual places and is properly configured, and that zabbix_sender is on the path somewhere.

Make a quick cron job w/ a 1 min interval to get 60s samples.


## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
