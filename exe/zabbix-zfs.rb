#!/usr/bin/env ruby
require 'zabbix_sender_api'
require 'optimist'
require 'json'
require 'pry'

hostname = Zabbix::AgentConfiguration.zabbixHostname
proxyaddr = Zabbix::AgentConfiguration.zabbixProxy
proxyaddr = '127.0.0.1' if not proxyaddr
procfsdir = '/proc/spl/kstat/zfs'

opts = Optimist::options do
        opt :procfsdir, "Path to the root of the zfs proc stuff", :type => :string, :default => procfsdir
	opt :hostname, "Name of host being monitored",:type=>:string, :default => hostname
	opt :proxy, "Proxy to send data to", :type => :string , :default => proxyaddr
	opt :dump
end

zfsproc = Dir.new(opts[:procfsdir])

zabbix_sender = Zabbix::Sender::Socket.new(proxy: opts[:proxy])
zabbixData = Zabbix::Sender::Batch.new(timestamp: Time.now, hostname: opts[:hostname])


poolDisco = Zabbix::Sender::Discovery.new(key: 'zpool')
pooldirs = Dir.glob(%Q(#{opts[:procfsdir]}/*)).select{|f| File.directory?(f)}
pooldirs.collect {|f| File.basename(f)}.each { |n|
  poolDisco.add_entity(:POOL => n)
}

fsDisco = Zabbix::Sender::Discovery.new(key: 'zfs')
pat = /(\S+)\s+\d\s+(\S+)/
pooldirs.each {|pooldir|
  state = File.read(%Q(#{pooldir}/state)).chomp
  zabbixData.addItemData(key: %Q(zpool_state[#{File.basename(pooldir)}]),value: state)
  io = File.readlines(%Q(#{pooldir}/io))
  io.shift # dump first line - don't know what's in it - no dox
  keys = io.shift.split(' ')
  values = io.shift.split(' ')
  poolstats = Hash[*keys.zip(values).flatten]
  poolstats.each_pair {|k,v|
    zabbixData.addItemData(key: %Q(zpool_#{k}[#{File.basename(pooldir)}]),value: v)
  }
  fss = Dir.glob(%Q(#{pooldir}/objset-*))
  fss.each { |fsentry|
    fsdata = File.readlines(fsentry).collect{|each|each.chomp}
    fsdata.shift(2) # dump first line + col header
    fsdata = Hash[*fsdata.collect{|each| pat.match(each).captures}.flatten]
    fsDisco.add_entity(:FS => fsdata['dataset_name'].tr('/','_'))
    fsdata.each_pair {|k,v|
      next if k == 'dataset_name'
      zabbixData.addItemData(key: %Q(zfs_#{k}[#{fsdata['dataset_name'].tr('/','_')}]),value: v)
    }
  }
}

zabbixData.addDiscovery(fsDisco)
zabbixData.addDiscovery(poolDisco)

if opts[:dump]
  puts zabbixData.to_senderline
else
  puts zabbix_sender.sendBatchAtomic(zabbixData)
end
